#include "core/func.h"

int main() {
    size_t length = 0;
    char *exp = NULL;
    if (get_line(&exp, &length)) {
        error();
        return 0;
    }

    set s = set_default;
    if (calc(exp, length, &s)) {
        free_arr((void **)&exp);
        error();
        return 0;
    }
    set_print(s);
    set_delete(&s);
    free(exp);

    return 0;
}
