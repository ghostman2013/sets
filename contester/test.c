#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char BOOL;

#define TRUE 1
#define FALSE 0

#define ARRAY_INIT 12
#define ARRAY_SMALL_INIT 4

/**
 * @brief Adds a new element to array
 * @param arr Pointer to the array pointer
 * @param val Pointer to a new element
 * @param t_size Size of an array element
 * @param size Pointer to the array size
 * @param capacity Pointer to array capacity
 * @return Not 0 if it has errors
 */
int add_el(void** arr, void* val, const size_t t_size,
           size_t* size, size_t* capacity);

/**
 * @brief Prints the "[error]" message into the default output stream
 */
void error();

/**
 * @brief Recycles an allocated memory
 * @param arr Pointer to the array
 */
void free_arr(void **arr);

/**
 * @brief Returns an inputed line by the user
 * @param str Pointer to string
 * @param size Pointer to string size
 * @return Not 0 if it has errors
 */
int get_line(char** str, size_t* size);

/**
 * @brief Initializes a new array
 * @param arr Pointer to the array pointer
 * @param t_size Type size
 * @param length Length of the array
 * @return Not 0 if it has errors
 */
int new_arr(void **arr, const size_t t_size, const size_t length);

/**
 * @brief Prints the string (/substring) in the default output stream
 * @param start Pointer to the start position of the substring
 * @param end Pointer to a position after the last element of the substring
 * @return Not 0 if it has errors
 */
int print_arr(char* start, char* end);

/**
 * @brief Removes an element by index from the array
 * @param arr Pointer to the array pointer
 * @param pos Element position
 * @param t_size Size of an array element
 * @param size Pointer to the array size
 * @param capacity Array capacity
 * @return Not 0 if it has errors
 */
int remove_el(void **arr, size_t pos, size_t t_size,
              size_t* size, const size_t capacity);
              
typedef struct string {
    size_t size;
    size_t capacity;
    char *data;
} string;

const string str_default = {0, 0, NULL};

/**
 * @brief Adds a new character in the string
 * @param ch Character
 * @return Not 0 if it has errors
 */
int str_add(string *str, const char ch);

/**
 * @brief Check if two strings are equal
 * @param lt Left string
 * @param rt Right string
 * @return 0 if strings aren't equal
 */
int str_cmp(const string lt, const string rt);

/**
 * @brief String comparator
 * @param p1 Pointer to the first string
 * @param p2 Pointer to the second string
 * @return Result (0 if strings are equal)
 */
int str_cmprtr(const void *p1, const void *p2);

/**
 * @brief Copies a source string to the destination string
 * @param src Source string
 * @param dst Pointer to the destination string
 * @return Not 0 if it has errors
 */
int str_copy(const string src, string *dst);

/**
 * @brief Recycles an allocated memory by the string
 * @param str Pointer to the string
 */
void str_delete(string *str);

/**
 * @brief Initializes a string by default values (doesn't free if a some memory
 * was allocated already)
 * @param str Pointer to the string
 * @return Not 0 if it has errors
 */
int str_new(string *str);

/**
 * @brief Removes a character by index in the string
 * @param str Pointer to the string
 * @param pos Character position
 * @return Not 0 if it has errors
 */
int str_remove(string *str, const size_t pos);

typedef struct set {
    size_t size;
    size_t capacity;
    struct string *str;
} set;

const set set_default = {0, 0, NULL};

/**
 * @brief Adds a new string in the set
 * @param str String
 * @return Not 0 if it has errors
 */
int set_add(set *st, const string str);

/**
 * @brief Recycles an allocated memory by the set
 * @param st Set
 */
void set_delete(set *st);

/**
 * @brief Returns the intersections of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_mul(set *lt, set rt);

/**
 * @brief Initializes a set by default values (doesn't free if a some memory
 * was allocated already)
 * @param set Pointer to the set
 * @return Not 0 if it has errors
 */
int set_new(set *set);

/**
 * @brief Prints a set in the default output stream
 * @param set Set
 */
void set_print(set set);

/**
 * @brief Removes a string by index from the set
 * @param st Pointer to the set
 * @param pos String index
 * @param recycle If it's TRUE an allocated memory for the element will be
 * released
 * @return Not 0 if it has errors
 */
int set_remove(set *st, const size_t pos, const BOOL recycle);

/**
 * @brief Returns the subtraction of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_sub(set *lt, set rt);

/**
 * @brief Returns the union of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_sum(set *lt, set rt);

/**
 * @brief Convert set to C string
 * @param st Pointer to the set
 * @return C string
 */
char *set_to_str(set *st);

#define UNI 'U'
#define INS '^'
#define SUB '\\'
#define LBR '('
#define RBR ')'
#define LSB '['
#define RSB ']'
#define COM ','
#define MRK '"'
#define SET 'S'

/**
 * @brief Calculates set expressions with union or subtraction as operations of
 * the lowest priority
 * @param in Input char array
 * @param length Length of the input array
 * @param result Result set
 * @return Not 0 if it has errors
 */
int calc(const char *in, const size_t length, set *result);

/**
 * @brief Adds a new element to array
 * @param arr Pointer to array pointer
 * @param val Pointer to a new element
 * @param t_size Size of array element
 * @param size Pointer to array size
 * @param capacity Pointer to array capacity
 * @return Not 0 if it has errors
 */
int add_el(void** arr, void* val, const size_t t_size, size_t* size,
           size_t* capacity) {
    if ((*size) == ((*capacity) - 1)) {
        *capacity <<= 1;
        void* p_tmp = *arr;
        *arr = realloc(*arr, (*capacity) * t_size);
        if (!(*arr)) {
            *arr = p_tmp;
            return 1;
        }
    }
    memcpy((*arr) + (*size) * t_size, val, t_size);
    ++(*size);

    return 0;
}

/**
 * @brief Prints the "[error]" message into the default output stream
 */
void error() {
    printf("[error]");
}

/**
 * @brief Recycles an allocated memory
 * @param arr Pointer to the array
 */
void free_arr(void **arr) {
    if (*arr) {
        free(*arr);
        *arr = NULL;
    }
}

/**
 * @brief Returns an inputed line by the user
 * @param str Pointer to string
 * @param size Pointer to string size
 * @return Not 0 if it has errors
 */
int get_line(char** str, size_t* size) {
    size_t capacity = ARRAY_INIT;
    *size = 0;
    char* s = (char*)malloc(capacity * sizeof(char));
    char ch;
    while (scanf("%c", &ch) == 1 && !(ch == '\0' || ch == '\n')) {
        if (add_el((void **)(&s), &ch, sizeof(char), size, &capacity)) {
            return 1;
        }
    }
    *str = (char *)malloc((*size) * sizeof(char));
    if (*str) { memcpy(*str, s, (*size) * sizeof(char)); }
    free(s);
    s = NULL;

    return 0;
}

/**
 * @brief Initializes a new array
 * @param arr Pointer to the array pointer
 * @param t_size Type size
 * @param length Length of the array
 * @return Not 0 if it has errors
 */
int new_arr(void **arr, const size_t t_size, const size_t length) {
    *arr = malloc(t_size * length);
    if (!arr) { return 1; }
    return 0;
}

/**
 * @brief Prints the string (/substring) in default output stream
 * @param start Pointer to the start position of the substring
 * @param end Pointer to a position after the last element of the substring
 * @return Not 0 if it has errors
 */
int print_arr(char* start, char* end) {
    if (!start || end < start) { return 1; }
    for (char *i = start; i < end; ++i) {
        printf("%c", *i);
    }

    return 0;
}

/**
 * @brief Removes an element by index from the array
 * @param arr Pointer to the array pointer
 * @param pos Element position
 * @param t_size Size of an array element
 * @param size Pointer to the array size
 * @param capacity Array capacity
 * @return Not 0 if it has errors
 */
int remove_el(void **arr, size_t pos, size_t t_size,
              size_t* size, const size_t capacity) {
    if (pos >= (*size)) { return 1; }
    void *tmp = malloc(capacity * t_size);
    if (!tmp) { return 1; }
    size_t offset = pos * t_size;
    size_t length = *size * t_size;
    memcpy(tmp, *arr, offset);
    memcpy(tmp + offset, (*arr) + offset + t_size, length - offset - t_size);
    free_arr(arr);
    *arr = tmp;
    --(*size);

    return 0;
}

/**
 * @brief Adds a new character in the string
 * @param ch Character
 * @return Not 0 if it has errors
 */
int str_add(string *str, const char ch) {
    if (!str || (!str->data && str_new(str))) { return 1; }
    if (add_el((void **)&str->data, (void *)&ch,
               sizeof(char), &str->size, &str->capacity)) { return 1; }
    str->data[str->size] = '\0';
    return 0;
}

/**
 * @brief Check if two strings are equal
 * @param lt Left string
 * @param rt Right string
 * @return 0 if strings aren't equal
 */
int str_cmp(const string lt, const string rt) {
    if (lt.size != rt.size) { return 0; }
    for (size_t i = 0; i < lt.size; ++i) {
        if (lt.data[i] != rt.data[i]) { return 0; }
    }
    return 1;
}

/**
 * @brief String comparator
 * @param p1 Pointer to the first string
 * @param p2 Pointer to the second string
 * @return Result (0 if strings are equal)
 */
int str_cmprtr(const void *p1, const void *p2) {
    string *s1 = (string *)p1;
    string *s2 = (string *)p2;
    return strcmp(s1->data, s2->data);
}

/**
 * @brief Copies a source string to the destination string
 * @param src Source string
 * @param dst Pointer to the destination string
 * @return Not 0 if it has errors
 */
int str_copy(const string src, string *dst) {
    dst->capacity = src.capacity;
    dst->size = src.size;
    if (!src.data || src.size == 0) { return 0; }
    dst->data = (char *)malloc(dst->capacity * sizeof(char));
    if (!dst->data) { return 1; }
    memcpy(dst->data, src.data, (dst->size + 1) * sizeof(char));
    return 0;
}

/**
 * @brief Recycles an allocated memory by the string
 * @param str Pointer to the string
 */
void str_delete(string *str) {
    if (str && str->data) {
        free(str->data);
        str->data = NULL;
        str->size = 0;
        str->capacity = 0;
    }
}

/**
 * @brief Initializes a string by default values (doesn't free if a some memory
 * was allocated already)
 * @param str Pointer to the string
 * @return Not 0 if it has errors
 */
int str_new(string *str) {
    str->size = 0;
    str->capacity = ARRAY_SMALL_INIT;
    if (new_arr((void **)&str->data, sizeof(char), str->capacity)) { return 1; }
    str->data[0] = '\0';
    return 0;
}

/**
 * @brief Removes a character by index in the string
 * @param str Pointer to the string
 * @param pos Character position
 * @return Not 0 if it has errors
 */
int str_remove(string *str, const size_t pos) {
    if (!str || !str->data || str->size <= pos) { return 1; }
    return remove_el((void **)&str->data, pos,
                     sizeof(char), &str->size, str->capacity);
}

/**
 * @brief Adds a new string in the set
 * @param str String
 * @return Not 0 if it has errors
 */
int set_add(set *st, const string str) {
    if (!st || (!st->str && set_new(st))) { return 1; }
    return add_el((void **)&st->str, (void *)&str,
                  sizeof(string), &st->size, &st->capacity);
}

/**
 * @brief Recycles an allocated memory by the set
 * @param st Set
 */
void set_delete(set *st) {
    if (st && st->str) {
        for (size_t i = 0; i < st->size; ++i) { str_delete(st->str + i); }
        free(st->str);
        st->str = NULL;
        st->size = 0;
        st->capacity = 0;
    }
}

/**
 * @brief Returns the intersections of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_mul(set* lt, set rt) {
    for (size_t i = 0; i < lt->size; ++i) {
        BOOL for_removing = TRUE;
        for (size_t j = 0; j < rt.size; ++j) {
            if (str_cmp(lt->str[i], rt.str[j])) {
                for_removing = FALSE;
                break;
            }
        }
        if (for_removing && set_remove(lt, i--, TRUE)) { return 1; }
    }
    return 0;
}

/**
 * @brief Initializes a set by default values (doesn't free if a some memory
 * was allocated already)
 * @param set Pointer to the set
 * @return Not 0 if it has errors
 */
int set_new(set *set) {
    set->size = 0;
    set->capacity = ARRAY_SMALL_INIT;
    return new_arr((void **)&set->str, sizeof(string), set->capacity);
}

/**
 * @brief Prints a set in the default output stream
 * @param set Set
 */
void set_print(set st) {
    qsort(st.str, st.size, sizeof(string), str_cmprtr);
    printf("[");
    if (st.str && st.size > 0) {
        const string *end = st.str + st.size - 1;
        for (string *i = st.str; i < end; ++i) {
            printf("\"");
            print_arr(i->data, i->data + i->size);
            printf("\",");
        }
        if (end >= st.str) {
            printf("\"");
            print_arr(end->data, end->data + end->size);
            printf("\"");
        }
    }
    printf("]");
}

/**
 * @brief Removes a string by index from the set
 * @param st Pointer to the set
 * @param pos String index
 * @param recycle If it's TRUE an allocated memory for the element will be
 * released
 * @return Not 0 if it has errors
 */
int set_remove(set *st, const size_t pos, const BOOL recycle) {
    if (!st || !st->str || st->size <= pos) { return 1; }
    if (recycle) { str_delete(st->str + pos); }
    return remove_el((void **)&st->str, pos,
                     sizeof(string), &st->size, st->capacity);
}

/**
 * @brief Returns the subtraction of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_sub(set *lt, set rt) {
    for (size_t i = 0; i < lt->size; ++i) {
        BOOL for_removing = FALSE;
        for (size_t j = 0; j < rt.size; ++j) {
            if (str_cmp(lt->str[i], rt.str[j])) {
                for_removing = TRUE;
                break;
            }
        }
        if (for_removing && set_remove(lt, i--, TRUE)) { return 1; }
    }
    return 0;
}

/**
 * @brief Returns the union of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_sum(set *lt, set rt) {
    for (size_t i = 0; i < rt.size; ++i) {
        BOOL for_adding = TRUE;
        for (size_t j = 0; j < lt->size; ++j) {
            if (str_cmp(rt.str[i], lt->str[j])) {
                for_adding = FALSE;
                break;
            }
        }
        if (for_adding) {
            string copy;
            if (str_copy(rt.str[i], &copy) || set_add(lt, copy)) {
                str_delete(&copy);
                return 1;
            }
        }
    }
    return 0;
}

/**
 * @brief Convert set to C string
 * @param st Pointer to the set
 * @return C string
 */
char *set_to_str(set *st) {
    qsort(st->str, st->size, sizeof(string *), str_cmprtr);
    size_t size = 3;
    for (size_t i = 0; i < st->size; ++i) {
        size += st->str[i].size + 2;
    }
    if (st->size) { size += st->size - 1; }
    char *arr = (char *)malloc(size * sizeof(char));
    if (!arr) { return NULL; }
    arr[0] = '[';
    arr[size - 1] = '\0';
    size_t idx = 1;
    for (size_t i = 0; i < st->size; ++i) {
        arr[idx++] = '\"';
        memcpy(arr + idx++, st->str[i].data, st->str[i].size);
        arr[idx++] = '\"';
        arr[idx++] = ',';
    }
    arr[size - 2] = ']';
    return arr;
}

/**
 * @brief Calculates set expressions with the intersection as operation of the
 * lowest priority
 * @param in Input char array
 * @param length Length of the input array
 * @param result Result set
 * @return Not 0 if it has errors
 */
int calc2(const char *in, const size_t length, set *result);

/**
 * @brief Parses a string ending by the (") terminal
 * @param in Input char array
 * @param length Length of the input array
 * @param it Current position in the input array (will be move to
 * an element in the input array that located after a terminal character)
 * @param str Output string
 * @return Not 0 if any errors
 */
int parse_chain(const char *in, const size_t length, size_t *it, string *str);

/**
 * @brief Parses the next chain into the set
 * @param in Input char array
 * @param length Length of the input array
 * @param st Set
 * @return Not 0 if any errors
 */
int parse_into_set(const char *in, const size_t length, size_t *it, set *st);

/**
 * @brief Parses a set ending by (]) terminal
 * @param in Input char array
 * @param length Length of the input array
 * @param it Current position in the input array (will be move to
 * an element in the input array that located after a terminal character)
 * @param st Parsed set
 * @return Not 0 if any errors
 */
int parse_set(const char *in, const size_t length, set *st);

int calc(const char *in, const size_t length, set *result) {
    if (!in || length == 0) { return 0; }
    BOOL is_last_brace = in[length - 1] == RBR;
    int counter = 0;
    for (int i = (int)length - 1; i >= 0; --i) {
        char ch = in[i];
        set r = set_default;
        switch (ch) {
        case LBR:
            if (counter >= 0) { return 1; }
            ++counter; break;
        case RBR: --counter; break;
        case UNI:
            if (counter) { break; }
            if (calc(in, i, result) || calc(in + i + 1, length - i - 1, &r)
                    || set_sum(result, r)) {
                set_delete(result);
                set_delete(&r);
                return 1;
            }
            set_delete(&r);
            return 0;
        case SUB:
            if (counter) { break; }
            if (calc(in, i, result) || calc(in + i + 1, length - i - 1, &r)
                    || set_sub(result, r)) {
                set_delete(result);
                set_delete(&r);
                return 1;
            }
            set_delete(&r);
            return 0;
        }
    }
    if (counter == 0 && is_last_brace && *in == LBR) {
        return calc(in + 1, length - 2, result);
    }
    return calc2(in, length, result);
}

int calc2(const char *in, const size_t length, set *result) {
    if (!in || length == 0) { return 1; }
    int counter = 0;
    for (int i = (int)length - 1; i >= 0; --i) {
        char ch = in[i];
        switch (ch) {
        case LBR:
            if (counter >= 0) { return 1; }
            ++counter; break;
        case RBR: --counter; break;
        case INS:
            if (counter) { break; }
            set r = set_default;
            if (calc(in, i, result) || calc(in + i + 1, length - i - 1, &r)
                    || set_mul(result, r)) {
                set_delete(result);
                set_delete(&r);
                return 1;
            }
            set_delete(&r);
            return 0;
        }
    }
    return parse_set(in, length, result);
}

int parse_chain(const char *in, const size_t length, size_t *it, string *str) {
    if (str_new(str)) { return 1; }
    char end = 0;
    for (; *it < length; ++(*it)) {
        char ch = in[*it];
        end = ch == MRK;
        if (end) {
            ++(*it);
            break;
        }
        if (str_add(str, ch)) {
            str_delete(str);
            return 1;
        }
    }
    if (!end) {
        str_delete(str);
        return 1;
    }

    return 0;
}

int parse_into_set(const char *in, const size_t length, size_t *it, set *st) {
    string element = str_default;
    return parse_chain(in, length, it, &element) || set_add(st, element);
}

int parse_set(const char *in, const size_t length, set *st) {
    if (!in || length < 2 || set_new(st) || *in != LSB) { return 1; }
    size_t commas = 0;
    unsigned char error = 0;
    for (size_t it = 1; it < length; ++it) {
        const char ch = in[it];
        switch (ch) {
        case RSB:
            if (commas || it != (length - 1)) { error = 1; }
            break;
        case MRK:
            ++it;
            if (parse_into_set(in, length, &it, st)) { error = 1; }
            commas = 0;
            break;
        case COM:
            ++commas;
            if (commas > 1) { error = 1; }
            break;
        default:
            error = 1;
        }
        if (error) {
            set_delete(st);
            return 1;
        }
    }

    return 0;
}

int main() {
    size_t length = 0;
    char *exp = NULL;
    if (get_line(&exp, &length)) {
        error();
        return 0;
    }

    struct set s = set_default;
    if (calc(exp, length, &s)) {
        free_arr((void **)&exp);
        error();
        return 0;
    }
    set_print(s);
    set_delete(&s);
    free(exp);

    return 0;
}
