#ifndef STRING_H
#define STRING_H

#include "io.h"

typedef struct string {
    size_t size;
    size_t capacity;
    char *data;
} string;

extern const string str_default;

/**
 * @brief Adds a new character in the string
 * @param ch Character
 * @return Not 0 if it has errors
 */
int str_add(string *str, const char ch);

/**
 * @brief Check if two strings are equal
 * @param lt Left string
 * @param rt Right string
 * @return 0 if strings aren't equal
 */
int str_cmp(const string lt, const string rt);

/**
 * @brief String comparator
 * @param p1 Pointer to the first string
 * @param p2 Pointer to the second string
 * @return Result (0 if strings are equal)
 */
int str_cmprtr(const void *p1, const void *p2);

/**
 * @brief Copies a source string to the destination string
 * @param src Source string
 * @param dst Pointer to the destination string
 * @return Not 0 if it has errors
 */
int str_copy(const string src, string *dst);

/**
 * @brief Recycles an allocated memory by the string
 * @param str Pointer to the string
 */
void str_delete(string *str);

/**
 * @brief Initializes a string by default values (doesn't free if a some memory
 * was allocated already)
 * @param str Pointer to the string
 * @return Not 0 if it has errors
 */
int str_new(string *str);

/**
 * @brief Removes a character by index in the string
 * @param str Pointer to the string
 * @param pos Character position
 * @return Not 0 if it has errors
 */
int str_remove(string *str, const size_t pos);

#endif // STRING_H
