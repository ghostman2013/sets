#include "set.h"

const set set_default = {0, 0, NULL};

/**
 * @brief Adds a new string in the set
 * @param str String
 * @return Not 0 if it has errors
 */
int set_add(set *st, const string str) {
    if (!st || (!st->str && set_new(st))) { return 1; }
    return add_el((void **)&st->str, (void *)&str,
                  sizeof(string), &st->size, &st->capacity);
}

/**
 * @brief Recycles an allocated memory by the set
 * @param st Set
 */
void set_delete(set *st) {
    if (st && st->str) {
        for (size_t i = 0; i < st->size; ++i) { str_delete(st->str + i); }
        free(st->str);
        st->str = NULL;
        st->size = 0;
        st->capacity = 0;
    }
}

/**
 * @brief Returns the intersections of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_mul(set* lt, set rt) {
    for (size_t i = 0; i < lt->size; ++i) {
        BOOL for_removing = TRUE;
        for (size_t j = 0; j < rt.size; ++j) {
            if (str_cmp(lt->str[i], rt.str[j])) {
                for_removing = FALSE;
                break;
            }
        }
        if (for_removing && set_remove(lt, i--, TRUE)) { return 1; }
    }
    return 0;
}

/**
 * @brief Initializes a set by default values (doesn't free if a some memory
 * was allocated already)
 * @param set Pointer to the set
 * @return Not 0 if it has errors
 */
int set_new(set *set) {
    set->size = 0;
    set->capacity = ARRAY_SMALL_INIT;
    return new_arr((void **)&set->str, sizeof(string), set->capacity);
}

/**
 * @brief Prints a set in the default output stream
 * @param set Set
 */
void set_print(set st) {
    qsort(st.str, st.size, sizeof(string), str_cmprtr);
    printf("[");
    if (st.str && st.size > 0) {
        const string *end = st.str + st.size - 1;
        for (string *i = st.str; i < end; ++i) {
            printf("\"");
            print_arr(i->data, i->data + i->size);
            printf("\",");
        }
        if (end >= st.str) {
            printf("\"");
            print_arr(end->data, end->data + end->size);
            printf("\"");
        }
    }
    printf("]");
}

/**
 * @brief Removes a string by index from the set
 * @param st Pointer to the set
 * @param pos String index
 * @param recycle If it's TRUE an allocated memory for the element will be
 * released
 * @return Not 0 if it has errors
 */
int set_remove(set *st, const size_t pos, const BOOL recycle) {
    if (!st || !st->str || st->size <= pos) { return 1; }
    if (recycle) { str_delete(st->str + pos); }
    return remove_el((void **)&st->str, pos,
                     sizeof(string), &st->size, st->capacity);
}

/**
 * @brief Returns the subtraction of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_sub(set *lt, set rt) {
    for (size_t i = 0; i < lt->size; ++i) {
        BOOL for_removing = FALSE;
        for (size_t j = 0; j < rt.size; ++j) {
            if (str_cmp(lt->str[i], rt.str[j])) {
                for_removing = TRUE;
                break;
            }
        }
        if (for_removing && set_remove(lt, i--, TRUE)) { return 1; }
    }
    return 0;
}

/**
 * @brief Returns the union of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_sum(set *lt, set rt) {
    for (size_t i = 0; i < rt.size; ++i) {
        BOOL for_adding = TRUE;
        for (size_t j = 0; j < lt->size; ++j) {
            if (str_cmp(rt.str[i], lt->str[j])) {
                for_adding = FALSE;
                break;
            }
        }
        if (for_adding) {
            string copy;
            if (str_copy(rt.str[i], &copy) || set_add(lt, copy)) {
                str_delete(&copy);
                return 1;
            }
        }
    }
    return 0;
}

/**
 * @brief Convert set to C string
 * @param st Pointer to the set
 * @return C string
 */
char *set_to_str(set *st) {
    qsort(st->str, st->size, sizeof(string *), str_cmprtr);
    size_t size = 3;
    for (size_t i = 0; i < st->size; ++i) {
        size += st->str[i].size + 2;
    }
    if (st->size) { size += st->size - 1; }
    char *arr = (char *)malloc(size * sizeof(char));
    if (!arr) { return NULL; }
    arr[0] = '[';
    arr[size - 1] = '\0';
    size_t idx = 1;
    for (size_t i = 0; i < st->size; ++i) {
        arr[idx++] = '\"';
        memcpy(arr + idx++, st->str[i].data, st->str[i].size);
        arr[idx++] = '\"';
        arr[idx++] = ',';
    }
    arr[size - 2] = ']';
    return arr;
}
