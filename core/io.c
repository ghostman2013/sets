#include "io.h"

/**
 * @brief Adds a new element to array
 * @param arr Pointer to array pointer
 * @param val Pointer to a new element
 * @param t_size Size of array element
 * @param size Pointer to array size
 * @param capacity Pointer to array capacity
 * @return Not 0 if it has errors
 */
int add_el(void** arr, void* val, const size_t t_size, size_t* size,
           size_t* capacity) {
    if ((*size) == ((*capacity) - 1)) {
        *capacity <<= 1;
        void* p_tmp = *arr;
        *arr = realloc(*arr, (*capacity) * t_size);
        if (!(*arr)) {
            *arr = p_tmp;
            return 1;
        }
    }
    memcpy((*arr) + (*size) * t_size, val, t_size);
    ++(*size);

    return 0;
}

/**
 * @brief Prints the "[error]" message into the default output stream
 */
void error() {
    printf("[error]");
}

/**
 * @brief Recycles an allocated memory
 * @param arr Pointer to the array
 */
void free_arr(void **arr) {
    if (*arr) {
        free(*arr);
        *arr = NULL;
    }
}

/**
 * @brief Returns an inputed line by the user
 * @param str Pointer to string
 * @param size Pointer to string size
 * @return Not 0 if it has errors
 */
int get_line(char **str, size_t *size) {
    size_t capacity = ARRAY_INIT;
    *size = 0;
    char *s = (char *)malloc(capacity * sizeof(char));
    char ch;
    while (scanf("%c", &ch) == 1 && !(ch == '\0' || ch == '\n')) {
        if (add_el((void **)(&s), &ch, sizeof(char), size, &capacity)) {
            return 1;
        }
    }
    *str = (char *)malloc((*size) * sizeof(char));
    if (*str) { memcpy(*str, s, (*size) * sizeof(char)); }
    free(s);
    s = NULL;

    return 0;
}

/**
 * @brief Initializes a new array
 * @param arr Pointer to the array pointer
 * @param t_size Type size
 * @param length Length of the array
 * @return Not 0 if it has errors
 */
int new_arr(void **arr, const size_t t_size, const size_t length) {
    *arr = malloc(t_size * length);
    if (!arr) { return 1; }
    return 0;
}

/**
 * @brief Prints the string (/substring) in default output stream
 * @param start Pointer to the start position of the substring
 * @param end Pointer to a position after the last element of the substring
 * @return Not 0 if it has errors
 */
int print_arr(char* start, char* end) {
    if (!start || end < start) { return 1; }
    for (char *i = start; i < end; ++i) {
        printf("%c", *i);
    }

    return 0;
}

/**
 * @brief Removes an element by index from the array
 * @param arr Pointer to the array pointer
 * @param pos Element position
 * @param t_size Size of an array element
 * @param size Pointer to the array size
 * @param capacity Array capacity
 * @return Not 0 if it has errors
 */
int remove_el(void **arr, size_t pos, size_t t_size,
              size_t* size, const size_t capacity) {
    if (pos >= (*size)) { return 1; }
    void *tmp = malloc(capacity * t_size);
    if (!tmp) { return 1; }
    size_t offset = pos * t_size;
    size_t length = *size * t_size;
    memcpy(tmp, *arr, offset);
    memcpy(tmp + offset, (*arr) + offset + t_size, length - offset - t_size);
    free_arr(arr);
    *arr = tmp;
    --(*size);

    return 0;
}
