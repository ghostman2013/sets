#ifndef FUNC_H
#define FUNC_H

#include "set.h"

#define UNI 'U'
#define INS '^'
#define SUB '\\'
#define LBR '('
#define RBR ')'
#define LSB '['
#define RSB ']'
#define COM ','
#define MRK '"'
#define SET 'S'

/**
 * @brief Calculates set expressions with union or subtraction as operations of
 * the lowest priority
 * @param in Input char array
 * @param length Length of the input array
 * @param result Result set
 * @return Not 0 if it has errors
 */
int calc(const char *in, const size_t length, set *result);

#endif // FUNC_H
