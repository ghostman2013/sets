#ifndef IO_H
#define IO_H

#include "pch.h"

#define ARRAY_INIT 12
#define ARRAY_SMALL_INIT 4

/**
 * @brief Adds a new element to array
 * @param arr Pointer to the array pointer
 * @param val Pointer to a new element
 * @param t_size Size of an array element
 * @param size Pointer to the array size
 * @param capacity Pointer to array capacity
 * @return Not 0 if it has errors
 */
int add_el(void** arr, void* val, const size_t t_size,
           size_t* size, size_t* capacity);

/**
 * @brief Prints the "[error]" message into the default output stream
 */
void error();

/**
 * @brief Recycles an allocated memory
 * @param arr Pointer to the array
 */
void free_arr(void **arr);

/**
 * @brief Returns an inputed line by the user
 * @param str Pointer to string
 * @param size Pointer to string size
 * @return Not 0 if it has errors
 */
int get_line(char** str, size_t* size);

/**
 * @brief Initializes a new array
 * @param arr Pointer to the array pointer
 * @param t_size Type size
 * @param length Length of the array
 * @return Not 0 if it has errors
 */
int new_arr(void **arr, const size_t t_size, const size_t length);

/**
 * @brief Prints the string (/substring) in the default output stream
 * @param start Pointer to the start position of the substring
 * @param end Pointer to a position after the last element of the substring
 * @return Not 0 if it has errors
 */
int print_arr(char* start, char* end);

/**
 * @brief Removes an element by index from the array
 * @param arr Pointer to the array pointer
 * @param pos Element position
 * @param t_size Size of an array element
 * @param size Pointer to the array size
 * @param capacity Array capacity
 * @return Not 0 if it has errors
 */
int remove_el(void **arr, size_t pos, size_t t_size,
              size_t* size, const size_t capacity);

#endif // IO_H
