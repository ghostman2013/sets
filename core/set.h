#ifndef SET_H
#define SET_H

#include "str.h"

typedef struct set {
    size_t size;
    size_t capacity;
    struct string *str;
} set;

extern const set set_default;

/**
 * @brief Adds a new string in the set
 * @param str String
 * @return Not 0 if it has errors
 */
int set_add(set *st, const string str);

/**
 * @brief Recycles an allocated memory by the set
 * @param st Set
 */
void set_delete(set *st);

/**
 * @brief Returns the intersections of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_mul(set *lt, set rt);

/**
 * @brief Initializes a set by default values (doesn't free if a some memory
 * was allocated already)
 * @param set Pointer to the set
 * @return Not 0 if it has errors
 */
int set_new(set *set);

/**
 * @brief Prints a set in the default output stream
 * @param set Set
 */
void set_print(set set);

/**
 * @brief Removes a string by index from the set
 * @param st Pointer to the set
 * @param pos String index
 * @param recycle If it's TRUE an allocated memory for the element will be
 * released
 * @return Not 0 if it has errors
 */
int set_remove(set *st, const size_t pos, const BOOL recycle);

/**
 * @brief Returns the subtraction of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_sub(set *lt, set rt);

/**
 * @brief Returns the union of sets
 * @param lt Pointer to the left set (the result will be saved here)
 * @param rt Right set
 * @return Not 0 if errors
 */
int set_sum(set *lt, set rt);

/**
 * @brief Convert set to C string
 * @param st Pointer to the set
 * @return C string
 */
char *set_to_str(set *st);

#endif // SET_H
