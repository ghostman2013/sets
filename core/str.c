#include "str.h"

const string str_default = {0, 0, NULL};

/**
 * @brief Adds a new character in the string
 * @param ch Character
 * @return Not 0 if it has errors
 */
int str_add(string *str, const char ch) {
    if (!str || (!str->data && str_new(str))) { return 1; }
    if (add_el((void **)&str->data, (void *)&ch,
               sizeof(char), &str->size, &str->capacity)) { return 1; }
    str->data[str->size] = '\0';
    return 0;
}

/**
 * @brief Check if two strings are equal
 * @param lt Left string
 * @param rt Right string
 * @return 0 if strings aren't equal
 */
int str_cmp(const string lt, const string rt) {
    if (lt.size != rt.size) { return 0; }
    for (size_t i = 0; i < lt.size; ++i) {
        if (lt.data[i] != rt.data[i]) { return 0; }
    }
    return 1;
}

/**
 * @brief String comparator
 * @param p1 Pointer to the first string
 * @param p2 Pointer to the second string
 * @return Result (0 if strings are equal)
 */
int str_cmprtr(const void *p1, const void *p2) {
    string *s1 = (string *)p1;
    string *s2 = (string *)p2;
    return strcmp(s1->data, s2->data);
}

/**
 * @brief Copies a source string to the destination string
 * @param src Source string
 * @param dst Pointer to the destination string
 * @return Not 0 if it has errors
 */
int str_copy(const string src, string *dst) {
    dst->capacity = src.capacity;
    dst->size = src.size;
    if (!src.data || src.size == 0) { return 0; }
    dst->data = (char *)malloc(dst->capacity * sizeof(char));
    if (!dst->data) { return 1; }
    memcpy(dst->data, src.data, (dst->size + 1) * sizeof(char));
    return 0;
}

/**
 * @brief Recycles an allocated memory by the string
 * @param str Pointer to the string
 */
void str_delete(string *str) {
    if (str && str->data) {
        free(str->data);
        str->data = NULL;
        str->size = 0;
        str->capacity = 0;
    }
}

/**
 * @brief Initializes a string by default values (doesn't free if a some memory
 * was allocated already)
 * @param str Pointer to the string
 * @return Not 0 if it has errors
 */
int str_new(string *str) {
    str->size = 0;
    str->capacity = ARRAY_SMALL_INIT;
    if (new_arr((void **)&str->data, sizeof(char), str->capacity)) { return 1; }
    str->data[0] = '\0';
    return 0;
}

/**
 * @brief Removes a character by index in the string
 * @param str Pointer to the string
 * @param pos Character position
 * @return Not 0 if it has errors
 */
int str_remove(string *str, const size_t pos) {
    if (!str || !str->data || str->size <= pos) { return 1; }
    return remove_el((void **)&str->data, pos,
                     sizeof(char), &str->size, str->capacity);
}
