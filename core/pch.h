#ifndef PCH_H
#define PCH_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char BOOL;

#define TRUE 1
#define FALSE 0

#endif // PCH_H
