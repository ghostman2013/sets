#include "func.h"

/**
 * @brief Calculates set expressions with the intersection as operation of the
 * lowest priority
 * @param in Input char array
 * @param length Length of the input array
 * @param result Result set
 * @return Not 0 if it has errors
 */
int calc2(const char *in, const size_t length, set *result);

/**
 * @brief Parses a string ending by the (") terminal
 * @param in Input char array
 * @param length Length of the input array
 * @param it Current position in the input array (will be move to
 * an element in the input array that located after a terminal character)
 * @param str Output string
 * @return Not 0 if any errors
 */
int parse_chain(const char *in, const size_t length, size_t *it, string *str);

/**
 * @brief Parses the next chain into the set
 * @param in Input char array
 * @param length Length of the input array
 * @param st Set
 * @return Not 0 if any errors
 */
int parse_into_set(const char *in, const size_t length, size_t *it, set *st);

/**
 * @brief Parses a set ending by (]) terminal
 * @param in Input char array
 * @param length Length of the input array
 * @param it Current position in the input array (will be move to
 * an element in the input array that located after a terminal character)
 * @param st Parsed set
 * @return Not 0 if any errors
 */
int parse_set(const char *in, const size_t length, set *st);

int calc(const char *in, const size_t length, set *result) {
    if (!in || length == 0) { return 0; }
    BOOL is_last_brace = in[length - 1] == RBR;
    int counter = 0;
    for (int i = (int)length - 1; i >= 0; --i) {
        char ch = in[i];
        set r = set_default;
        switch (ch) {
        case LBR:
            if (counter >= 0) { return 1; }
            ++counter; break;
        case RBR: --counter; break;
        case UNI:
            if (counter) { break; }
            if (calc(in, i, result) || calc(in + i + 1, length - i - 1, &r)
                    || set_sum(result, r)) {
                set_delete(result);
                set_delete(&r);
                return 1;
            }
            set_delete(&r);
            return 0;
        case SUB:
            if (counter) { break; }
            if (calc(in, i, result) || calc(in + i + 1, length - i - 1, &r)
                    || set_sub(result, r)) {
                set_delete(result);
                set_delete(&r);
                return 1;
            }
            set_delete(&r);
            return 0;
        }
    }
    if (counter == 0 && is_last_brace && *in == LBR) {
        return calc(in + 1, length - 2, result);
    }
    return calc2(in, length, result);
}

int calc2(const char *in, const size_t length, set *result) {
    if (!in || length == 0) { return 1; }
    int counter = 0;
    for (int i = (int)length - 1; i >= 0; --i) {
        char ch = in[i];
        switch (ch) {
        case LBR:
            if (counter >= 0) { return 1; }
            ++counter; break;
        case RBR: --counter; break;
        case INS:
            if (counter) { break; }
            set r = set_default;
            if (calc(in, i, result) || calc(in + i + 1, length - i - 1, &r)
                    || set_mul(result, r)) {
                set_delete(result);
                set_delete(&r);
                return 1;
            }
            set_delete(&r);
            return 0;
        }
    }
    return parse_set(in, length, result);
}

int parse_chain(const char *in, const size_t length, size_t *it, string *str) {
    if (str_new(str)) { return 1; }
    char end = 0;
    for (; *it < length; ++(*it)) {
        char ch = in[*it];
        end = ch == MRK;
        if (end) {
            ++(*it);
            break;
        }
        if (str_add(str, ch)) {
            str_delete(str);
            return 1;
        }
    }
    if (!end) {
        str_delete(str);
        return 1;
    }

    return 0;
}

int parse_into_set(const char *in, const size_t length, size_t *it, set *st) {
    string element = str_default;
    return parse_chain(in, length, it, &element) || set_add(st, element);
}

int parse_set(const char *in, const size_t length, set *st) {
    if (!in || length < 2 || set_new(st) || *in != LSB) { return 1; }
    size_t commas = 0;
    unsigned char error = 0;
    for (size_t it = 1; it < length; ++it) {
        const char ch = in[it];
        switch (ch) {
        case RSB:
            if (commas || it != (length - 1)) { error = 1; }
            break;
        case MRK:
            ++it;
            if (parse_into_set(in, length, &it, st)) { error = 1; }
            commas = 0;
            break;
        case COM:
            ++commas;
            if (commas > 1) { error = 1; }
            break;
        default:
            error = 1;
        }
        if (error) {
            set_delete(st);
            return 1;
        }
    }

    return 0;
}
