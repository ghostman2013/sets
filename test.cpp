#include <gtest/gtest.h>
#include <cstring>
extern "C" {
#include "core/func.h"
}

class SetsTestSuite : public testing::Test {
    void SetUp() { }
    void TearDown() { }
};

TEST_F(SetsTestSuite, Calc) {
    auto in = "([\"1\",\"2\",\"3\"]U[\"1\",\"2\",\"4 2\",\"8\"]U[])^[\"1\",\"8\",\"3\",\"1 3\"]";
    auto expected = "[\"1\",\"3\",\"8\"]";
    size_t length = strlen(in);

    struct set s;
    ASSERT_FALSE(calc(in, length, &s));
    char *actual = set_to_str(&s);
    ASSERT_TRUE(actual);
    ASSERT_STREQ(expected, actual);

    set_delete(&s);
}

TEST_F(SetsTestSuite, Calc2) {
    auto in = "([\"3\",\"2\",\"1\"]U[\"1\",\"2\",\"4 2\",\"8\"]U[])^[\"8\",\"1\",\"3\",\"1 3\"]";
    auto expected = "[\"1\",\"3\",\"8\"]";
    size_t length = strlen(in);

    struct set s;
    ASSERT_FALSE(calc(in, length, &s));
    char *actual = set_to_str(&s);
    ASSERT_TRUE(actual);
    ASSERT_STREQ(expected, actual);

    set_delete(&s);
}

TEST_F(SetsTestSuite, Calc3) {
    auto in = "[\"3\",\"5\",\"7\",\"9\",\"11\",\"13\",\"15\"]^([\"2\",\"4\",\"6\",\"8\",\"10\",\"12\",\"14\"]U[\"8\",\"64\"]";
    auto expected = "[error]";
    size_t length = strlen(in);

    struct set s;
    ASSERT_FALSE(calc(in, length, &s));
    char *actual = set_to_str(&s);
    ASSERT_TRUE(actual);
    ASSERT_STREQ(expected, actual);

    set_delete(&s);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
    return 0;
}
